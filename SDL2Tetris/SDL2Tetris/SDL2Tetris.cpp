﻿#include <SDL.h>
#include <iostream>

enum {I, J, L, O, S, T, Z};

const int tetraminos[7][4] = {
	{1, 3, 5, 7},
	{2, 4, 5, 6},
	{1, 3, 5, 6},
	{1, 2, 3, 4},
	{1, 3, 4, 6},
	{1, 3, 4, 5},
	{2, 3, 4, 5}
};

struct Figure {
	int t;
	int dir;
	SDL_Rect size;
};

void get_coords(Figure f, SDL_Point coords[4]) {
	for (int i = 0; i < 4; ++i) {
		int e = tetraminos[f.t][i] - 1;
		int y = e / 2;
		int x = e - y * 2;

		if (f.dir == 90) {
			int tp = y;
			y = x;
			x = tp;
			y = 1 - y;
		} else if (f.dir == 180) {
			y = 2 - y;
			x = 1 - x;
		} else if (f.dir == 270) {
			int tp = y;
			y = x;
			x = 2 - tp;
			y = y;
		}
		coords[i].x = x;
		coords[i].y = y;
	}
}

void rotate(Figure &f, int dir) {
	f.dir = (360 + (f.dir + dir)) % 360;
}

int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 Tetris", 30, 30, 600, 600, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	Figure figures[7];
	int y = 2;
	for (int i = 0; i < 7; ++i) {
		int x = (i % 3) * 6 + 4;
		if (i % 3 == 0 && i != 0) {
			y += 6;
		}
		figures[i].dir = 0;
		figures[i].t = i;
		figures[i].size.x = x;
		figures[i].size.y = y;
		figures[i].size.w = figures[i].size.h = 30;
	}
	SDL_Point coords[4];

	unsigned int stamp = SDL_GetTicks();
	bool exit = false;
	while (exit != true) {
		unsigned int s = SDL_GetTicks();
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				exit = true;
				break;
			}
		}

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		for (int i = 0; i < 600 / 30; ++i) {
			SDL_RenderDrawLine(renderer, i * 30, 0, i * 30, 600);
		}

		for (int i = 0; i < 600 / 30; ++i) {
			SDL_RenderDrawLine(renderer, 0, i * 30, 800, i * 30);
		}

		for (int i = 0; i < 7; ++i) {
			get_coords(figures[i], coords);
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_Rect quad = { 0, 0, figures[i].size.w, figures[i].size.h };
			for (int j = 0; j < 4; ++j) {
				quad.x = (coords[j].x + figures[i].size.x) * figures[i].size.w;
				quad.y = (coords[j].y + figures[i].size.y) * figures[i].size.h;
				SDL_RenderFillRect(renderer, &quad);
			}
		}
		
		SDL_RenderPresent(renderer);

		stamp += SDL_GetTicks() - s;
		if (stamp > 1000) {
			stamp = 0;
			for (int i = 0; i < 7; ++i) {
				int d = rand() % 2;
				if (d == 0) d = -1;
				rotate(figures[i], 90);
			}
		}
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}