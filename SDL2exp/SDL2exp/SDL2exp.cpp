﻿#include <SDL.h>
#include <iostream>

int main(int argc, char *argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window *window = SDL_CreateWindow("SDL2 Lesson 2", 30, 30, 800, 600, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	bool exit = false;
	while (exit != true) {
		SDL_SetRenderDrawColor(renderer, 200, 0, 0, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				exit = true;
				break;
			}
		}

		SDL_Rect rect1;
		rect1.x = 375;
		rect1.y = 275;
		rect1.w = 50;
		rect1.h = 50;
		SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);
		SDL_RenderFillRect(renderer, &rect1);
		SDL_RenderPresent(renderer);
	}


	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
