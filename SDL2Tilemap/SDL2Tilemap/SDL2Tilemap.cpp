﻿#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

#include <cstdio>
#include "..//tinyxml2/tinyxml2.h"

struct TileSet {
	SDL_Texture* tilemap_image; // тайлмап
	int tile_width, tile_height; // ширина и высота одного тайла в пикселях
	int spacing; // расстояние в пикселях между тайлами
	int firstid; // айди первого тайла
	int tilecount; // количество тайлов в текстуре
	int columns; // столбцов в текстуре
};

struct TileMap {
	int** tiles; // массив айдишников тайлов
	TileSet tilesets[5];
	int tileset_count;
	int map_width, map_height; // ширина и высота карты в тайлах
};

/*
	Функция загрузки текстуры

	rend - контекст окна
	file - путь к картинке
	background - фоновый цвет картинки,
	для картинок с прозрачным фоном не играет роли

	Возвращает текстуру в случае успешной загрузки, иначе nullptr
*/
SDL_Texture* loadTexture(SDL_Renderer* rend, const char* file, SDL_Color background) {
	SDL_Surface* tsurf = IMG_Load(file);

	if (tsurf == nullptr) {
		return nullptr;
	}
	// Переводим цвет из формата RGB в формат картинки
	unsigned int transparent = SDL_MapRGB(tsurf->format, background.r, background.g, background.b);
	// Задаем этот цвет ключевым, т.е. его можно считать прозрачным и не рисовать
	SDL_SetColorKey(tsurf, SDL_TRUE, transparent);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(rend, tsurf);
	SDL_FreeSurface(tsurf);
	return texture;
}

TileSet parseTileSet(SDL_Renderer *rend, tinyxml2::XMLElement* tile_element) {
	using namespace tinyxml2;

	TileSet set;
	set.firstid = 0;

	const char* firstid_str = tile_element->Attribute("firstgid");
	if (firstid_str) {
		set.firstid = atoi(firstid_str);
	}

	const char* tilewidth_str = tile_element->Attribute("tilewidth");
	if (tilewidth_str) {
		set.tile_width = atoi(tilewidth_str);
	}

	const char* tileheight_str = tile_element->Attribute("tileheight");
	if (tileheight_str) {
		set.tile_height = atoi(tileheight_str);
	}

	const char* spacing_str = tile_element->Attribute("spacing");
	if (spacing_str) {
		set.spacing = atoi(spacing_str);
	}

	const char* tilecount_str = tile_element->Attribute("tilecount");
	if (tilecount_str) {
		set.tilecount = atoi(tilecount_str);
	}

	const char* columns_str = tile_element->Attribute("columns");
	if (columns_str) {
		set.columns = atoi(columns_str);
	}

	XMLElement* image_element = tile_element->FirstChildElement("image");

	if (image_element) {
		const char* path = image_element->Attribute("source");
		if (path) {
			SDL_Color back;
			back.r = 255;
			back.g = 0;
			back.b = 0;
			set.tilemap_image = loadTexture(rend, path, back);
		}
	} else {
		set.tilemap_image = nullptr;
	}
	return set;
}

TileMap* parseTileMap(SDL_Renderer* rend, const char* file) {
	using namespace tinyxml2;

	TileMap* map = new TileMap;
	FILE* f = fopen(file, "rb");
	XMLDocument *doc = new XMLDocument;
	doc->LoadFile(f);
	XMLElement* map_element = doc->FirstChildElement("map");

	map->tiles = nullptr;
	const char* width_str = map_element->Attribute("width");
	if (width_str) {
		map->map_width = atoi(width_str);
	} else {
		map->map_width = 0;
	}

	const char* height_str = map_element->Attribute("height");
	if (height_str) {
		map->map_height = atoi(height_str);
	} else {
		map->map_height = 0;
	}

	if (map->map_width && map->map_height) {
		XMLNode* child = nullptr;
		map->tileset_count = 0;
		while (1) {
			child = map_element->FirstChild();

			if (strcmp(child->ToElement()->Name(), "layer") == 0)
				break;
			
			map->tilesets[map->tileset_count] = parseTileSet(rend, child->ToElement());
			map_element->DeleteChild(child);
			map->tileset_count++;
		}

		XMLElement* layer_element = map_element->FirstChildElement("layer");
		if (layer_element) {
			XMLElement* data_element = layer_element->FirstChildElement();
			const char* _s = data_element->GetText();

			char* csv_data = new char[strlen(_s) + 1];
			memcpy(csv_data, _s, strlen(_s) + 1);
			char* tile_s = strtok(csv_data, ",");
			
			map->tiles = new int* [map->map_height];
			for (int i = 0; i < map->map_height; ++i) {
				map->tiles[i] = new int[map->map_width];
				for (int j = 0; j < map->map_width; ++j) {
					if (tile_s == nullptr) {
						break;
					}
					map->tiles[i][j] = atoi(tile_s);
					tile_s = strtok(nullptr, ",");
				}
			}
			delete[] csv_data;
		}
	}

	delete doc;
	fclose(f);
	return map;
}

TileSet getTileSet(int tile, TileSet* sets, int set_count) {
	int i;
	for (i = 0; i < set_count; ++i) {
		if (tile > sets[i].firstid && tile < sets[i].firstid + sets[i].tilecount)
			break;
	}
	return sets[i];
}

void drawTileMap(SDL_Renderer* rend, TileMap* map, float scale) {
	for (int i = 0; i < map->map_height; ++i) {
		for (int j = 0; j < map->map_width; ++j) {
			if (map->tiles[i][j] == 0)
				continue;
			TileSet set = getTileSet(map->tiles[i][j], map->tilesets, map->tileset_count);
			int tile = map->tiles[i][j] - set.firstid;
			SDL_Rect coords;
			coords.x = j * set.tile_width * scale;
			coords.y = i * set.tile_height * scale;
			coords.w = set.tile_width * scale;
			coords.h = set.tile_height * scale;
			SDL_Rect tile_rect;
			int y = tile / set.columns;
			int x = tile - y * set.columns;
			tile_rect.x = x * (set.tile_width + set.spacing);
			tile_rect.y = y * (set.tile_height + set.spacing);
			tile_rect.w = set.tile_width;
			tile_rect.h = set.tile_height;
			SDL_RenderCopy(rend, set.tilemap_image, &tile_rect, &coords);
		}
	}
}

void destroyTileMap(TileMap* map) {
	for (int i = 0; i < map->map_height; ++i) {
		delete[] map->tiles[i];
	}
	delete[] map->tiles;
	for (int i = 0; i < map->tileset_count; ++i) {
		SDL_DestroyTexture(map->tilesets[i].tilemap_image);
	}
	delete map;
}

int main(int argc, char** argv) {
	IMG_Init(IMG_INIT_PNG);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 Tilemap", 30, 30, 1000, 800, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}
	/*SDL_Color back;
	back.r = 255;
	back.g = 0;
	back.b = 0;
	SDL_Texture* adventurer = loadTexture(renderer, "adventurer-sheet_red.png", back);*/
	TileMap* map = parseTileMap(renderer, "map.tmx");

	bool exit = false;
	while (exit != true) {
		SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				exit = true;
				break;
			}
		}
		drawTileMap(renderer, map, 0.5);
		SDL_RenderPresent(renderer);
	}

	destroyTileMap(map);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
	return 0;
}
