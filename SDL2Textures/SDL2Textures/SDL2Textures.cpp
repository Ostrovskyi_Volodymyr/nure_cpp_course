﻿#include <SDL.h>
#include <iostream>

enum state {NORMAL, PRESSED};

struct {
	SDL_Texture* tile;
	SDL_Texture* click;
} resources;

int loadResources(SDL_Renderer* renderer) {
	SDL_Surface* stemp = SDL_LoadBMP("tile.bmp");
	resources.tile = SDL_CreateTextureFromSurface(renderer, stemp);
	if (resources.tile == nullptr) {
		return -1;
	}
	stemp = SDL_LoadBMP("click.bmp");
	resources.click = SDL_CreateTextureFromSurface(renderer, stemp);
	if (resources.click == nullptr) {
		return -1;
	}
	return 0;
}

int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 Lesson 2", 30, 30, 500, 500, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	int ret = loadResources(renderer);

	if (ret < 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	bool exit = false;
	SDL_Texture* currentTexture = resources.tile;
	char field[10][10] = {};
	while (exit != true) {
		SDL_SetRenderDrawColor(renderer, 200, 0, 0, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				exit = true;
				break;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN) {
				if (event.button.button == SDL_BUTTON_LEFT) {
					int x = event.button.x / 50;
					int y = event.button.y / 50;
					field[y][x] = PRESSED;
				}
			}
			if (event.type == SDL_MOUSEBUTTONUP) {
				if (event.button.button == SDL_BUTTON_LEFT) {
					currentTexture = resources.tile;
				}
			}

		}

		SDL_Rect rect1;
		rect1.w = 50;
		rect1.h = 50;

		for (int y = 0; y < 10; y++) {
			for (int x = 0; x < 10; x++) {
				rect1.x = x * rect1.w;
				rect1.y = y * rect1.h;
				if (field[y][x] == NORMAL) {
					currentTexture = resources.tile;
				} else if (field[y][x] == PRESSED) {
					currentTexture = resources.click;
				}
				SDL_RenderCopy(renderer, currentTexture, 0, &rect1);
			}
		}
		//SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);
		//SDL_RenderFillRect(renderer, &rect1);
		SDL_RenderPresent(renderer);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
