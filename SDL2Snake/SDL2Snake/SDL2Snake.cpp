﻿#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <cmath>
#include <ctime>

enum { NONE, APPLE, BODY };

struct Block {
	int x, y;
};

struct Snake {
	Block body[15];
	int block_count;
	int dx, dy;
};

/*
	Функция загрузки текстуры

	rend - контекст окна
	file - путь к картинке
	background - фоновый цвет картинки,
	для картинок с прозрачным фоном не играет роли

	Возвращает текстуру в случае успешной загрузки, иначе nullptr
*/
SDL_Texture* loadTexture(SDL_Renderer* rend, const char* file, SDL_Color background) {
	SDL_Surface* tsurf = IMG_Load(file);

	if (tsurf == nullptr) {
		return nullptr;
	}
	// Переводим цвет из формата RGB в формат картинки
	unsigned int transparent = SDL_MapRGB(tsurf->format, background.r, background.g, background.b);
	// Задаем этот цвет ключевым, т.е. его можно считать прозрачным и не рисовать
	SDL_SetColorKey(tsurf, SDL_TRUE, transparent);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(rend, tsurf);
	SDL_FreeSurface(tsurf);
	return texture;
}

void game_init(Snake *snake, int field[10][10]) {
	snake->block_count = 3;
	snake->body[0].x = 5;
	snake->body[0].y = 8;
	snake->body[1].x = 4;
	snake->body[1].y = 8;
	snake->body[2].x = 3;
	snake->body[2].y = 8;
	snake->dx = 1;
	snake->dy = 0;

	for (int y = 0; y < 10; ++y) {
		for (int x = 0; x < 10; ++x) {
			field[y][x] = NONE;
		}
	}

	int x = rand() % 10;
	int y = rand() % 10;
	field[y][x] = APPLE;
}

void draw_field(SDL_Renderer *rend, SDL_Texture *g, SDL_Texture *a, int field[10][10]) {
	SDL_Rect t = { 0, 0, 50, 50 };
	for (int y = 0; y < 10; ++y) {
		for (int x = 0; x < 10; ++x) {
			t.x = x * t.w;
			t.y = y * t.h;
			SDL_RenderCopy(rend, g, 0, &t);
			if (field[y][x] == APPLE) {
				SDL_RenderCopy(rend, a, 0, &t);
			}
		}
	}
}

void draw_snake(SDL_Renderer *rend, Snake *snake) {
	SDL_Rect t = { 0, 0, 50, 50 };
	for (int i = 0; i < snake->block_count; ++i) {
		t.x = snake->body[i].x * t.w;
		t.y = snake->body[i].y * t.h;
		SDL_SetRenderDrawColor(rend, 155, 0, 0, 255);
		SDL_RenderFillRect(rend, &t);
	}
}

void move_snake(Snake *snake) {
	static unsigned int stamp = SDL_GetTicks();
	static unsigned int tm = 0;
	unsigned int s = SDL_GetTicks();
	tm += s - stamp;
	if (tm >= 100) {
		tm = 0;
		for (int i = snake->block_count - 1; i > 0; i--) {
			snake->body[i].x = snake->body[i - 1].x;
			snake->body[i].y = snake->body[i - 1].y;
		}

		snake->body[0].x += snake->dx;
		snake->body[0].y += snake->dy;

		int &hx = snake->body[0].x;
		int &hy = snake->body[0].y;

		if (hx > 9) {
			hx = 0;
		}
		else if (hx < 0) {
			hx = 9;
		}
		if (hy > 9) {
			hy = 0;
		}
		else if (hy < 0) {
			hy = 9;
		}
	}
	stamp = SDL_GetTicks();
}

void snake_add_tail(Snake *snake) {
	int x = snake->body[snake->block_count - 1].x - snake->dx;
	int y = snake->body[snake->block_count - 1].y - snake->dy;
	snake->body[snake->block_count].x = x;
	snake->body[snake->block_count].y = y;
	snake->block_count++;
}

int check_collision(Snake *snake, int field[10][10]) {

	int hx = snake->body[0].x;
	int hy = snake->body[0].y;

	for (int i = 1; i < snake->block_count; ++i) {
		int bx = snake->body[i].x;
		int by = snake->body[i].y;
		if (bx == hx && by == hy) {
			return BODY;
		}
	}

	if (field[hy][hx] == APPLE) {
		field[hy][hx] = NONE;
		snake_add_tail(snake);
		return APPLE;
	}

	return false;
}

int main(int argc, char **argv) {
	srand(time(nullptr));
	IMG_Init(IMG_INIT_PNG);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 Animations", 30, 30, 500, 500, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	Snake snake;
	int field[10][10] = { NONE };
	game_init(&snake, field);

	SDL_Color cl = { 255, 255, 255 };
	SDL_Texture *apple_tx = loadTexture(renderer, "apple.png", cl);
	SDL_Texture *grass_tx = loadTexture(renderer, "grass.png", cl);
	bool exit = false;

	while (exit != true) {
		SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				exit = true;
				break;
			}
			if (event.type == SDL_KEYDOWN) {
				switch (event.key.keysym.sym) {
				case SDLK_UP: {
					if (snake.dy != 0)
						break;
					snake.dx = 0;
					snake.dy = -1;
					break;
				}
				case SDLK_DOWN: {
					if (snake.dy != 0)
						break;
					snake.dx = 0;
					snake.dy = 1;
					break;
				}
				case SDLK_LEFT: {
					if (snake.dx != 0)
						break;
					snake.dx = -1;
					snake.dy = 0;
					break;
				}
				case SDLK_RIGHT: {
					if (snake.dx != 0)
						break;
					snake.dx = 1;
					snake.dy = 0;
					break;
				}
				}
			}
		}
		move_snake(&snake);
		int s = check_collision(&snake, field);
		if (s == BODY) {
			game_init(&snake, field);
		}
		else if (s == APPLE) {
			int x = rand() % 10;
			int y = rand() % 10;
			field[y][x] = APPLE;
		}
		draw_field(renderer, grass_tx, apple_tx, field);
		draw_snake(renderer, &snake);
		SDL_RenderPresent(renderer);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
	return 0;
}