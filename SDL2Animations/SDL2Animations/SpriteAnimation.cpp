#include "SpriteAnimation.h"

SpriteAnimation* createAnimation(SDL_Texture* sheet, int spr_w, int spr_h) {
	SpriteAnimation* anim = new SpriteAnimation;
	anim->sprite_w = spr_w;
	anim->sprite_h = spr_h;
	anim->spritesheet = sheet;
	anim->current_state = 0;
	anim->state_count = 0;
	anim->elapsedTime = 0;
	anim->timestamp = 0;
	return anim;
}

SpriteState* createSpriteState(unsigned int rate_msec, int frame_w, int frame_h) {
	SpriteState* state = new SpriteState;
	state->rate_msec = rate_msec;
	state->current_frame = 0;
	state->frame_count = 0;
	state->frame_w = frame_w;
	state->frame_h = frame_h;
	return state;
}

void addFrame(SpriteState* state, int sx, int sy) {
	state->frames[state->frame_count][0] = sx;
	state->frames[state->frame_count][1] = sy;
	state->frame_count++;
}

void addState(SpriteAnimation* anim, SpriteState* state) {
	anim->states[anim->state_count] = state;
	anim->state_count++;
}

void setState(SpriteAnimation* anim, int state) {
	if (state < anim->state_count) {
		anim->current_state = state;
		anim->elapsedTime = 0;
		anim->timestamp = 0;
	}
}

void drawAnimation(SDL_Renderer* renderer, SpriteAnimation* anim, SDL_Rect target) {
	if (anim->timestamp == 0) {
		anim->timestamp = SDL_GetTicks();
	}
	unsigned int t2 = SDL_GetTicks();
	anim->elapsedTime += t2 - anim->timestamp;
	anim->timestamp = t2;
	SpriteState* curr_state = anim->states[anim->current_state];
	if (anim->elapsedTime > curr_state->rate_msec) {
		anim->elapsedTime = 0;
		curr_state->current_frame = 
			(curr_state->current_frame + 1) % curr_state->frame_count;
	}
	SDL_Rect frame_rect;
	frame_rect.x = curr_state->frames[curr_state->current_frame][0] * curr_state->frame_w;
	frame_rect.y = curr_state->frames[curr_state->current_frame][1] * curr_state->frame_h;
	frame_rect.w = curr_state->frame_w;
	frame_rect.h = curr_state->frame_h;
	SDL_RenderCopy(renderer, anim->spritesheet, &frame_rect, &target);
}

void destroyAnimation(SpriteAnimation* anim) {
	for (int i = 0; i < anim->state_count; i++) {
		delete anim->states[i];
	}
	delete anim;
}