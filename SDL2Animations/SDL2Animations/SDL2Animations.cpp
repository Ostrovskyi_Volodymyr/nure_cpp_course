﻿#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

#include "SpriteAnimation.h"

/*
	Функция загрузки текстуры

	rend - контекст окна
	file - путь к картинке
	background - фоновый цвет картинки,
	для картинок с прозрачным фоном не играет роли

	Возвращает текстуру в случае успешной загрузки, иначе nullptr
*/
SDL_Texture* loadTexture(SDL_Renderer* rend, const char* file, SDL_Color background) {
	SDL_Surface* tsurf = IMG_Load(file);

	if (tsurf == nullptr) {
		return nullptr;
	}
	// Переводим цвет из формата RGB в формат картинки
	unsigned int transparent = SDL_MapRGB(tsurf->format, background.r, background.g, background.b);
	// Задаем этот цвет ключевым, т.е. его можно считать прозрачным и не рисовать
	SDL_SetColorKey(tsurf, SDL_TRUE, transparent);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(rend, tsurf);
	SDL_FreeSurface(tsurf);
	return texture;
}

int main(int argc, char **argv) {
	IMG_Init(IMG_INIT_PNG);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 Animations", 30, 30, 500, 500, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}
	SDL_Color back;
	back.r = 255;
	back.g = 0;
	back.b = 0;
	SDL_Texture* adventurer = loadTexture(renderer, "adventurer-sheet_red.png", back);
	SpriteAnimation* anim = createAnimation(adventurer, 0, 0);
	SpriteState* idle = createSpriteState(100, 50, 37);
	addFrame(idle, 0, 0);
	addFrame(idle, 1, 0);
	addFrame(idle, 2, 0);
	addFrame(idle, 3, 0);
	addState(anim, idle);

	bool exit = false;
	while (exit != true) {
		SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				exit = true;
				break;
			}
		}
		SDL_Rect target;
		target.x = 100;
		target.y = 100;
		target.w = 100;
		target.h = 72;
		drawAnimation(renderer, anim, target);
		SDL_RenderPresent(renderer);
	}

	destroyAnimation(anim);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
	return 0;
}
