#pragma once

#include <SDL.h>
#include <ctime>

struct SpriteState {
	int frames[30][2];
	unsigned int rate_msec;
	int frame_w, frame_h;
	int current_frame, frame_count;
};

struct SpriteAnimation {
	SpriteState* states[10];
	int sprite_w, sprite_h;
	int current_state, state_count;
	SDL_Texture* spritesheet;
	unsigned int elapsedTime, timestamp;
};

SpriteAnimation* createAnimation(SDL_Texture* sheet, int spr_w, int spr_h);
SpriteState* createSpriteState(unsigned int rate_msec, int frame_w, int frame_h);
void addFrame(SpriteState* state, int sx, int sy);
void addState(SpriteAnimation* anim, SpriteState* state);
void setState(SpriteAnimation* anim, int state);
void drawAnimation(SDL_Renderer* renderer, SpriteAnimation* anim, SDL_Rect target);
void destroyAnimation(SpriteAnimation* anim);