﻿// AwesomeGame.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <SDL.h>
#include <ctime>

struct player {
	SDL_Rect body;
	float dx, dy;
};

struct bullet {
	SDL_Rect body;
	float dx, dy;
};

bool interception(SDL_Rect a, SDL_Rect b) {
	// Левый верхний
	if (a.x >= b.x && a.x <= b.x + b.w &&
		a.y >= b.y && a.y <= b.y + b.h) {
		return true;
	}

	// Правый верхний
	if (a.x + a.w >= b.x && a.x + a.w <= b.x + b.w &&
		a.y >= b.y && a.y <= b.y + b.h) {
		return true;
	}

	// Правый нижний
	if (a.x + a.w >= b.x && a.x + a.w <= b.x + b.w &&
		a.y + a.h >= b.y && a.y + a.h <= b.y + b.h) {
		return true;
	}

	if (a.x >= b.x && a.x <= b.x + b.w &&
		a.y + a.h >= b.y && a.y + a.h <= b.y + b.h) {
		return true;
	}
	return false;
}

int main(int argc, char **argv)
{
	srand(time(0));
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Window *window = SDL_CreateWindow("SDL2 Lesson 2", 30, 30, 800, 600, 0);

	if (window == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr) {
		std::cout << SDL_GetError();
		return -1;
	}

	player p1;
	p1.body.x = p1.body.y = 100;
	p1.body.w = p1.body.h = 40;
	p1.dx = 2;
	p1.dy = 5;
	char r = 255, g = 255, b = 255;

	bullet bullets[10];
	int next_pos = 0;

	for (int i = 0; i < 10; i++) {
		bullets[i].body.w = bullets[i].body.h = 15;
		bullets[i].dx = bullets[i].dy = 0;
	}

	bool exit = false;
	while (exit != true) {
		SDL_SetRenderDrawColor(renderer, 75, 75, 75, 255);
		SDL_RenderClear(renderer);

		SDL_Event event;
		while (SDL_PollEvent(&event)) {

			switch (event.type) {
			case SDL_QUIT: exit = true; break;
			case SDL_KEYDOWN: {
				if (event.key.keysym.sym == SDLK_LEFT) {
					p1.dx = -5;
					//p1.dy = 0;
				}
				if (event.key.keysym.sym == SDLK_RIGHT) {
					p1.dx = 5;
					//p1.dy = 0;
				}
				if (event.key.keysym.sym == SDLK_UP) {
					//p1.dx = 0;
					p1.dy = -5;
				}
				if (event.key.keysym.sym == SDLK_DOWN) {
					//p1.dx = 0;
					p1.dy = 5;
				}
				break;
			}
			case SDL_MOUSEBUTTONDOWN: {
				int x = event.button.x;
				int y = event.button.y;
				bullets[next_pos].body.x = x;
				bullets[next_pos].body.y = y;
				bullets[next_pos].dy = -10;
				next_pos = (next_pos + 1) % 10;
				break;
			}
			}
		}

		if (p1.body.y < 0 || p1.body.y + p1.body.h > 600) {
			p1.dy = -p1.dy * 1.03;
		}

		if (p1.body.x < 0 || p1.body.x + p1.body.w > 800) {
			p1.dx = -p1.dx * 1.03;
		}

		p1.body.x = p1.body.x + p1.dx;
		p1.body.y = p1.body.y + p1.dy;

		for (int i = 0; i < 10; i++) {
			if (bullets[i].dx == 0 && bullets[i].dy == 0)
				continue;
			if (interception(bullets[i].body, p1.body)) {
				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;
				bullets[i].dx = 0;
				bullets[i].dy = 0;
			}
		}
		//p1.dx = p1.dy = 0;

		SDL_SetRenderDrawColor(renderer, r, g, b, 255);
		SDL_RenderFillRect(renderer, &p1.body);

		SDL_SetRenderDrawColor(renderer, 150, 0, 0, 255);
		for (int i = 0; i < 10; i++) {
			if (bullets[i].dx == 0 && bullets[i].dy == 0)
				continue;
			bullets[i].body.x = bullets[i].body.x + bullets[i].dx;
			bullets[i].body.y = bullets[i].body.y + bullets[i].dy;
			SDL_RenderFillRect(renderer, &bullets[i].body);
		}

		SDL_RenderPresent(renderer);
	}


	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
